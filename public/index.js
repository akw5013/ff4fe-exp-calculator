let availableGrinds = {}
let startingLevels = {}
let availableGoals = {}
let characterData = {}

let flags = { }
let activeCharacters = { }
let activeCharacterLevels = { }
let characterGoals = { }
let goals = { }
let grindMethod = ""
let goalModifiers = { }
//let characterData = { }
modifyFlags = () => {
    flags = { }
    const theFlags = document.getElementById('flagsInput').value.toUpperCase()
    for(let i = 0; i < theFlags.length; i++) {
        //Check if the current position is alpha, if so it's a flag
        const currentIndex = i  //Set because we might modify i later 
        if(theFlags[i].match(/[A-Z\$]/g)) {
            //Check the next position to see if it's numeric
            let value = "1"
            if(i != theFlags.length - 1) {
                if(theFlags[i + 1].match(/[0-9]/g)) {
                    value = theFlags[i + 1]
                    //Skip the next value since we know it's not a flag value
                    i++
                }
            }
            flags[theFlags[currentIndex]] = value
        }
    }
    updateFlagDependentFields()
}
updateFlagDependentFields = () => {
    document.getElementById("DoubleExp").hidden = !(flags["X"] >= 2)
}
swapImage = (img) => {
    //If 5 party members already selected, don't allow for more    
    if(img.src.match(/_2/)) {
        img.src = img.src.replace(/_2/, "_1")
        delete activeCharacters[img.id]        
        delete activeCharacterLevels[img.id]
    } else if (Object.keys(activeCharacters).length < 5) {
        img.src = img.src.replace(/_1/, "_2")
        activeCharacters[img.id] = true        
    }
    updateCharacters()
    updatePossibleGoals()
}
updateCharacters = () => {
    document.getElementById("CurrentLevels").innerHTML = buildPartyLevelInput()
}
updateCheckboxGoal = (input) => {
    updateGoal(input.id, document.getElementById(input.id).checked)
}
updateTextGoal = (input) => {
    updateGoal(input.id, document.getElementById(input.id).value)
}
updateGoalModifiers = (modifier) => {
    goalModifiers[modifier.id] = modifier.checked
    calculateGoals()
}
updateGoal = (id, value) => {
    if(value) {
        goals[id] = value
    } else {
        delete goals[id]
    }    
    calculateGoals()
}
hasBlackMage = () => {
    return activeCharacters["Rydia"] === true || activeCharacters["Palom"] === true
}
hasWhiteMage = () => {
    return activeCharacters["Rosa"] === true || activeCharacters["Porom"] === true
}
updatePossibleGoals = () => {
    let whiteSpells = Object.keys(availableGoals).filter(spell => availableGoals[spell].White === true)
    let blackSpells = Object.keys(availableGoals).filter(spell => availableGoals[spell].Black === true)
    for(const whiteSpell of whiteSpells) {
        document.getElementById(`${whiteSpell}Goal`).hidden = !hasWhiteMage()
    }
    for(const blackSpell of blackSpells) {
        document.getElementById(`${blackSpell}Goal`).hidden = !hasBlackMage()
    }
}
updateGrind = (input) => {
    grindMethod = input.id
    calculateGoals()
}
buildGoalsInput = () => {
    let divs = []
    divs.push("<h4>Select your goals:</h4>")
    for(const goal of Object.keys(availableGoals)) {
        let div = ""
        const { type, comparison } = availableGoals[goal]      
        switch(type) {
            case "STAT":
                div = `<div id="${goal}Goal">${goal}: <input id="${goal}" type="text" onkeyup="updateTextGoal(this)" /> (${getDescriptionForComparison(comparison)})</div>`
                break
            case "SPELL":
                div = `<div id="${goal}Goal" hidden="true">${goal}: <input id="${goal}" type="checkbox" onchange="updateCheckboxGoal(this)" /></div>`
                break
        }
        divs.push(div)
    }
    return divs.join("\n")
}
getDescriptionForComparison = (comparison) => {
    let description = ""
    switch(comparison) {
        case "=":
            description = "Equals"
            break
        case "<=":
            description = "At Least"
            break
        case ">=":
            description = "At Most"
            break
        case ">":
            description = "Less Than"
            break
        case "<":
            description = "More Than"
            break
    }  
    return description
}
buildGrindOptionsInput = () => {
    let divs = []
    for(const grind of Object.keys(availableGrinds)) {
        let div = `<div>${grind}: <input id="${grind}" name="grind" type="radio" onchange="updateGrind(this)" /></div>`
        divs.push(div)
    }    
    return divs.join("\n")
}
buildPartyLevelInput = () => {
    let divs = []
    for(const character of Object.keys(activeCharacters)) {
        //Sets a default and the populates it back in the map if it's not there
        let currentLevel = activeCharacterLevels[character] || startingLevels[character]
        activeCharacterLevels[character] = currentLevel
        let div = `<div id="${character}LevelCurrent">${character}: <input id="${character}LevelInput" type="text" onchange="onCharacterLevelChange(this)" value="${currentLevel}" /></div>`
        divs.push(div)
    }
    return divs.join("\n")
}
onCharacterLevelChange = (input) => {
    const value = document.getElementById(input.id).value
    const key = input.id.replace("LevelInput", "")
    activeCharacterLevels[key] = value
}
calculateGoals = () => {
    if(!grindMethod || Object.keys(goals).length === 0) {
        return
    }
    //First calculate required levels for each goal, then figure out the min level required
    //Then figure out the required XP to hit that level
    let levelGoals = { }
    for(const goal of Object.keys(goals)) {        
        switch(availableGoals[goal].type) {
            case "SPELL":
                levelGoals = calculateSpellLevels(goal, levelGoals)
                break
            case "STAT":
            default:
                levelGoals = calculateStatLevels(goal, goals[goal], levelGoals)
                break
        }
    }
    processLevelGoals(levelGoals)
}
processLevelGoals = (levelGoals) => {
    const xpModifier = goalModifiers["DoubleExperience"] === true ? 2 : 1
    const lifeModifier = goalModifiers["LifeGlitch"] === true
    let xpPerGrindFight = (availableGrinds[grindMethod].baseXp + (lifeModifier ? availableGrinds[grindMethod].lifeXp : 0)) * xpModifier

    for(const character of Object.keys(levelGoals)) {
        const maxLevel = Math.max(...levelGoals[character])
        const charDataForCharacter = characterData[character]
        let currentLevelForCharacter = activeCharacterLevels[character]
        if(currentLevelForCharacter > 99) {
            //HP was entered, try to estimate the level based on the HP
            currentLevelForCharacter = calculateMinStatLevel(character, "HP", currentLevelForCharacter, ">=")
            console.log("Adjusted level based on HP, approximate level is " + currentLevelForCharacter)
        }
        const xpForLevel = charDataForCharacter[maxLevel]["EXPERIENCE"]
        const currentXp = charDataForCharacter[currentLevelForCharacter]["EXPERIENCE"]
        const sumNeeded = Math.max(xpForLevel - currentXp, 0)
        //TODO: Adjust for X0 flags
        let fightsNeeded = Math.round(sumNeeded / xpPerGrindFight)
        if(availableGrinds[grindMethod].limit && fightsNeeded > availableGrinds[grindMethod].limit) {
            fightsNeeded = "<b>IMPOSSIBLE</b>"
        }
        characterGoals[character] = { level: maxLevel, numberOfFights: fightsNeeded, grind: availableGrinds[grindMethod] } 
    }
    updateResultsDiv()
}
calculateSpellLevels = (goal, levelGoals) => {
    for(const character of Object.keys(activeCharacters)) {
        if(availableGoals[goal][character]) {
            let lvl = flags.J == 2 ? "J2" : "J"
            addLevelToGoalList(character, availableGoals[goal][character][lvl], levelGoals)
        }
    }
    return levelGoals
}
calculateStatLevels = (goal, target, levelGoals) => {
    return calculateMinStatLevels(goal, target, levelGoals, availableGoals[goal].comparison)
}
calculateMinStatLevel = (character, stat, target, operator) => {
    let minValue = 9999, maxValue = 0
    let firstLevel = Object.keys(characterData[character]).find(levelData => {
            const statAtLevel = characterData[character][levelData][stat]
            if(minValue > statAtLevel) { minValue = statAtLevel }
            if(maxValue < statAtLevel) { maxValue = statAtLevel }
            switch(operator) {
                case "=":
                default:
                    return target === statAtLevel
                case "<=":
                    return target <= statAtLevel
                case ">=":
                    return target >= statAtLevel
                case ">":
                    return target > statAtLevel
                case "<":
                    return target < statAtLevel
            }            
        })
    if(target < minValue) { firstLevel = startingLevels[character] } //Character starts with that much
    if(target > maxValue) { firstLevel = 99 } //Impossible to achieve
    return firstLevel
}
calculateMinStatLevels = (goal, target, levelGoals, operator) => {
    for(const character of Object.keys(activeCharacters)) {
        const firstLevel = calculateMinStatLevel(character, goal, target, operator)
        addLevelToGoalList(character, firstLevel, levelGoals)
    }
    return levelGoals 
}
addLevelToGoalList = (key, value, levelGoals) => {
    if(levelGoals[key]) {
        levelGoals[key].push(value)
    } else {
        levelGoals[key] = [value]
    }
}
updateResultsDiv = () => {
    let divs = []
    for(const character of Object.keys(characterGoals)) {
        let numOf = characterGoals[character].grind.repeatable ? "Summons" : "Fights"
        let div = `<div> ${character}: Target Level = ${characterGoals[character].level}, # of ${numOf} = ${characterGoals[character].numberOfFights}</div>`
        divs.push(div)
    }    
    document.getElementById("GoalResults").innerHTML = divs.join("\n")
}
loadFile = (location, cb) => {
    return fetch(location)
     .then(response => response.text())
     .then(text => {
         cb(JSON.parse(text))
     })
}
/* Raw Data */
loadData = () => {
    let promises = []
    promises.push(loadFile('./resources/characterdata/CharacterExperienceData.json', (data) => characterData = data))
    promises.push(loadFile('./resources/goaldata/available-grinds.json', data => availableGrinds = data))
    promises.push(loadFile('./resources/characterdata/starting-levels.json', data => startingLevels = data))
    promises.push(loadFile('./resources/goaldata/spell-goals.json', data => availableGoals = Object.assign(availableGoals, data)))
    promises.push(loadFile('./resources/goaldata/stat-goals.json', data => availableGoals = Object.assign(availableGoals, data)))
    Promise.all(promises).then(() => {
        document.getElementById("LoadingDiv").hidden = true
        document.getElementById("Goals").innerHTML = buildGoalsInput()
        document.getElementById("GrindOptions").innerHTML = buildGrindOptionsInput()        
    })
}
// readTextFile = async (file) => {
//     var rawFile = new XMLHttpRequest()
//     rawFile.open("GET", file, true)
//     rawFile.onreadystatechange = () => {
//         if(rawFile.readyState === 4) {
//             if(rawFile.status === 200 || rawFile.status == 0) {
//                 var allText = rawFile.responseText
//                 return allText
//             }
//         }
//     }
// }
// This function was used to read in the text file and parse it, but now that we have the json this isn't needed anymore
// loadCharacterMap = (text) => {
//     console.log("Loading Dayta")
//     const lines = text.split("\n")
//     const headers = lines[0].split(',')
//     const rows = lines.splice(1, lines.length)
//     console.log(headers)
//     for(const row of rows) {
//         const dataRows = row.split(',')        
//         const charName = dataRows[0]
//         const charLevel = dataRows[1]  
//         let charData = { }      
//         for(let i = 2; i < dataRows.length; i++) {
//             const header = headers[i].replace("\r", "")
//             const value = dataRows[i].replace("\r", "")
//             charData[header] = value
//         }        
//         if(!characterData[charName]) {
//             characterData[charName] = { }
//         }
//         characterData[charName][charLevel] = charData
//     }
//     console.log("Finished Loading Dayta")
//     console.log(JSON.stringify(characterData))      
// }